package main

import (
	"os"
	"fmt"
)

func main() {
	dir, err := os.Getwd()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		os.Exit(1)
	}
	fmt.Println(dir)

	os.Exit(0)
}
