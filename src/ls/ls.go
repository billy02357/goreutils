package main

import (
	"fmt"
	"os"
	flag "github.com/spf13/pflag"
	"log"
	"strings"
)

const program_version string = "0.1.0"
const program_name string = "ls"
const program_authors string = "Biel Sala"

var dir_color = "\033[1;34m" /* Blue */
var reset_color = "\033[0m"

var all_flag = flag.BoolP("all", "a", false, "Do not ignore entries starting with '.'")
var color_flag = flag.BoolP("color", "c", true, "Print with colors")
var list_flag = flag.BoolP("list", "l", false, "Print in list format")
var help_flag = flag.BoolP("help", "h", false, "Print help message")
var version_flag = flag.BoolP("version", "V", false, "Print version")
var one_flag = flag.BoolP("1", "1", false, "Print one file per line")

var n_of_flags = 6

var flag_names_and_usage = []string{ "-a, --all", "Do not ignore entries starting with '.'", "-c=<true/false>, --color=<true/false>", "Print with colors (default true)", "-l, --list", "Print in list format", "-h, --help", "Print this help message", "-V, --version", "Print version", "-1", "Print one file per line" }

func list_file_or_dir(f os.DirEntry, sep string) { /* Print files with no color, print dirs with dir_color if color_flag is true */
	if f.IsDir() {
		if *color_flag {
			fmt.Printf("%s%v%s%s", dir_color, f.Name(), reset_color, sep)
		} else {
			fmt.Printf("%v%s", f.Name(), sep)
		}
	} else {
		fmt.Printf("%v%s", f.Name(), sep)
	}
}

func print_perms(name string) {
	info, err := os.Stat(name)
	if err != nil {
		panic(err)
	}
	mode := info.Mode()
	var modtime_hours string
	var modtime_minutes string

	modtime_h := info.ModTime().Hour()
	if modtime_h < 10 {
		modtime_hours = fmt.Sprintf("0%d", modtime_h)
	} else {
		modtime_hours = fmt.Sprintf("%d", modtime_h)
	}

	modtime_m := info.ModTime().Minute()
	if modtime_m < 10 {
		modtime_minutes = fmt.Sprintf("0%d", modtime_m)
	} else {
		modtime_minutes = fmt.Sprintf("%d", modtime_m)
	}

	modtime_mon := fmt.Sprintf("%s", info.ModTime().Month())
	var modtime_month string
	switch (len(modtime_mon)) {
		case 9:
			modtime_month = fmt.Sprintf("%s", modtime_mon)
			break
		case 8:
			modtime_month = fmt.Sprintf("%s ", modtime_mon)
			break
		case 7:
			modtime_month = fmt.Sprintf("%s  ", modtime_mon)
			break
		case 6:
			modtime_month = fmt.Sprintf("%s   ", modtime_mon)
			break
		case 5:
			modtime_month = fmt.Sprintf("%s    ", modtime_mon)
			break
		case 4:
			modtime_month = fmt.Sprintf("%s     ", modtime_mon)
			break
		case 3:
			modtime_month = fmt.Sprintf("%s      ", modtime_mon)
			break
	}


	modtime_d := info.ModTime().Day()
	var modtime_day string
	if modtime_d < 10 {
		modtime_day = fmt.Sprintf("0%d", modtime_d)
	} else {
		modtime_day = fmt.Sprintf("%d", modtime_d)
	}
	fmt.Printf("%s  %s %s %s:%s  ", mode, modtime_month, modtime_day, modtime_hours, modtime_minutes) // -rwxrwxrwx November 12 15:45 ........
}

func print_help() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", program_name)
	for i := 0; i < n_of_flags*2; i++ {
		fmt.Fprintf(os.Stderr, "  %s\t", flag_names_and_usage[i])
		i++
		fmt.Fprintf(os.Stderr, "%s\n", flag_names_and_usage[i])
	}
	os.Exit(1)
}

func print_version() {
	fmt.Printf("%s %s\n", program_name, program_version)
	fmt.Println("License GPLv3: GNU GPL version 3 only <https://gnu.org/licenses/gpl.html>.")
	fmt.Println("This is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.")
	fmt.Printf("\nWritten by %s.\n", program_authors)
	os.Exit(0)
}

func main() {
	flag.Parse()

	if *help_flag {
		print_help()
	}
	if *version_flag {
		print_version()
	}

	path := flag.Arg(0)
	if path == "" {
		path = "."
	}
	if strings.HasPrefix(path, "~") {
		path = path[1:]
		npath := os.Getenv("HOME")
		npath += "/"
		npath += path
		path = npath
	}

	var sep string
	if *list_flag || *one_flag {
		sep = "\n"
	} else {
		sep = "  "
	}

	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	os.Chdir(path)
	if *all_flag {
		if *list_flag {
			print_perms(".")
		}
		fmt.Printf("%s.%s%s", dir_color, reset_color, sep)
		if *list_flag {
			print_perms("..")
		}
		fmt.Printf("%s..%s%s", dir_color, reset_color, sep)
		for _, f := range files {
			if *list_flag {
				print_perms(f.Name())
			}
			list_file_or_dir(f, sep)
		}
		if !*list_flag && !*one_flag {
			fmt.Println()
		}
	} else {
		for _, f := range files {
			if strings.HasPrefix(f.Name(), ".") {
				continue
			}
			if *list_flag {
				print_perms(f.Name())
			}
			list_file_or_dir(f, sep)
		}
		if !*list_flag && !*one_flag {
			fmt.Println()
		}
	}
}
