package main

import (
	"os"
	"fmt"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Error: No argument specified\n")
		os.Exit(1)
	}

	for i := 1; i < len(os.Args); i++ {
		fmt.Println(os.Getenv(os.Args[i]))
	}
	os.Exit(0)
}
