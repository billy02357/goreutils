package main

import (
	"os"
	"fmt"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println()
		os.Exit(0)
	}
	if os.Args[1] == "-n" {
		i := 0
		for _, arg := range os.Args[2:] {
			i++
			fmt.Printf("%s", arg)
			if i < (len(os.Args)-2) {
				fmt.Print(" ")
			}
		}
	} else {
		i := 0
		for _, arg := range os.Args[1:] {
			i++
			fmt.Printf("%s", arg)
			if i < (len(os.Args)-1) {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}

	os.Exit(0)
}
