#!/bin/sh

make_ls() {
	cd src/ls
	go build -o ../../bin/
	echo Built "ls" in bin/ls
}

make_pwd() {
	cd src/pwd
	go build -o ../../bin/
	echo Built "pwd" in bin/pwd
}

make_echo() {
	cd src/echo
	go build -o ../../bin/
	echo Built "echo" in bin/echo
}

make_printenv() {
	cd src/printenv
	go build -o ../../bin/
	echo Built "printenv" in bin/printenv
}

make_clean() {
	/bin/rm bin/*
}

print_make_list() {
	echo "Options: (./make.sh [option])"
	echo "  ls"
	echo "  pwd"
	echo "  echo"
	echo "  printenv"
	echo "  clean"
}

main() {
	if test "$1" = "ls"; then
		make_ls
	elif test "$1" = "pwd"; then
		make_pwd
	elif test "$1" = "echo"; then
		make_echo
	elif test "$1" = "printenv"; then
		make_printenv
	elif test "$1" = "clean"; then
		make_clean 2>/dev/null
	else
		echo "Error: No option"
		print_make_list
		exit 1
	fi
}

main "$@"
